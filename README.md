# Welcome to the VueJS workshop

Welcome to the first part of the vue-workshop.
Now it's time to clone the `vue-workshop` repo:

`git clone https://bitbucket.org/divotion/vue-workshop.git`

## 1.0 Your first vue instance!

We will start with a clean static web project.
Please checkout the branch of the first part of the vue-workshop using the command:
`git checkout step-1.0`

Open the `index.html` in the root of the project. As you can see, it contains a reference to a Bootstrap stylesheet and the vue.js library.

We will now create our first vue instance.
Modify the file `app.js` to create a vue instance:

`app.js`

```js
var app = new Vue({
  el: "#app",
  data: function() {
    return {};
  }
});
```

Now we add some data to our data object:

`app.js`

```js
var app = new Vue({
  el: "#app",
  data: function() {
    return {
        title: 'De parels onder de bieren',
        beers: [
            {
              brewery: 'brouwerij Jopen',
              title: 'Mooie Nel I.P.A.'
            },
            {
              brewery: 'brouwerij Jopen',
              title: 'Malle Babbe'
            }
        ]
    };
  }
});
```

Now we can use the Vue template-syntax to bind our data in the html.

For displaying a list we will use the [v-for directive](https://vuejs.org/v2/guide/list.html#v-for-with-an-Object)

For displaying a property in the html we can use the [interpolation with {{}}](https://vuejs.org/v2/guide/syntax.html)

`index.html`

```html
<!DOCTYPE html>
<html>
<head>
<title>Vue Workshop</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
    <div id="app">
        <div class="container mt-5">
            <div class="row">
                <h1>{{title}}</h1>
                <ul>
                    <li v-for="beer in beers">{{beer.title}} - {{beer.brewery}}</li>
                </ul>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="./app.js"></script>
</body>
</html>
```

If you now open your browser you see your first Vue-component in action!

## 1.1 Your first vue component!

If you would like to see the solution for the last part, checkout the `step-1.1` branch using the command:
`git checkout -f step-1.1`

Bigger web-projects can be seperated in components to increase maintainability. We are going to try to follow this pattern with our static Vue app.

Since we would like to enhance our 'beer-part', we first need to extract our 'beer-code' into a separate component.

- First we create a new js file: `beer.js` to create a new re-usable component.
- Don't forget to add your new javascript file to `index.html`

`index.html`
```html
<!DOCTYPE html>
<html>
  <head>
    <title>Vue Workshop</title>
    <link
      rel="stylesheet"
      href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
      integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
      crossorigin="anonymous"
    />
    <link rel="stylesheet" href="./style.css" />
  </head>
  <body>
    <div id="app">
      <div class="container mt-5">
        <div class="row">
          <h1>{{title.toLocaleUpperCase() }}</h1>
          <ul>
            <beer v-for="beer in beers" :beer="beer" />
          </ul>
        </div>
      </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="./beer.js"></script>
    <script src="./app.js"></script>
  </body>
</html>
```

Let's first create our new Vue-component in `beer.js`.

`beer.js`

```js
Vue.component("beer", {
  props: {
    beer: Object
  },
  data: function() {
    return {
      active: false
    };
  }
});
```

As you can see, we have added the property `props` to this component.
[Props](https://vuejs.org/v2/guide/components-props.html) are based on the One-Way dataflow to update data in a child-component. If the data from your prop changed in the parent, it's also changed in the child-component. But not the other way around.

The `active` data property will be used later on!.

First, we will add an extra property `percentage` to our `beers` array in our parent component.

`app.js`

```js
var app = new Vue({
  el: "#app",
  data: function() {
    return {
        title: 'De parels onder de bieren',
        beers: [
            {
              brewery: "brouwerij Jopen",
              title: "Mooie Nel I.P.A.",
              percentage: 6.5
            },
            {
              brewery: "brouwerij Jopen",
              title: "Malle Babbe",
              percentage: 4.5
            },
            {
              brewery: "De Leckere",
              title: "Witte Vrouwen",
              percentage: 6.5
            }
        ]
    };
  }
});
```

Next, we'll add some extra functionality to our `beer-component`.

`beer.js`

```js
Vue.component("beer", {
  props: {
    beer: Object
  },
  data: function() {
    return {
      active: false
    };
  },
  methods: {
    mouseOver: function() {
      this.active = !this.active;
    }
  },
  computed: {
    highPercentage: function() {
      if (this.beer.percentage > 5) {
        return true;
      } else {
        return false;
      }
    },
    highPercentageClass: function() {
      if (this.highPercentage) {
        return 'danger';
      } else {
        return '';
      }
    }
  },
  template: `<div>test</div>`
});
```

As you can see we have used some new Vue-powers.

[Vue-methods](https://vuejs.org/v2/api/#methods) are used to modify data in the component based on interactions.

[Computed properties](https://vuejs.org/v2/guide/computed.html#ad) are used to make properties based on logic, usable in the template. Of course these operations are also possible to do in-template. By using computed properties you'll keep your templates cleaner and you'll improve performance based on its cache functionality.

Now we will add the real template for our beer vue-component inside a template property.

`beer.js`

```js
Vue.component("beer", {
  props: {
    beer: Object
  },
  data: function() {
    return {
      active: false
    };
  },
  methods: {
    mouseOver: function() {
      this.active = !this.active;
    }
  },
  computed: {
    highPercentage: function() {
      if (this.beer.percentage > 5) {
        return true;
      } else {
        return false;
      }
    },
    highPercentageClass: function() {
      if (this.highPercentage) {
        return 'danger';
      } else {
        return '';
      }
    }
  },
  template:`
  <div class="beer-container">
      <span>{{beer.title}}</span> -
      <span>{{beer.brewery}}</span> -
      <span @mouseover="mouseOver" @mouseout="mouseOver" :class="highPercentageClass">{{ beer.percentage.toFixed() }} %</span>
      <div :class="highPercentageClass" class="tool-tip" v-if="active">
          <span v-if="highPercentage">
              Dit bier bevat een hoog percentage alcohol en kan schadelijk zijn voor de gezondheid.
          </span>
          <span v-else>
              Dit bier bevat alcohol.
          </span>
      </div>
  </div>`
});
```

As you may have noticed, we are using vue-directives. To get the directives up and running in the beer-component, add the implementation to your component for the following parts:

- The `mouseOver` method
- the `active` property in the data of the component to toggle the percentage information
- the `highPercentage` and the `highPercentageClass` computed-property. We will use a computed property, because the method is only called once and is cached until the specific dependencies change. Again, you can read more about the computed-property [here](https://vuejs.org/v2/guide/computed.html)

Dont't forget to add the beer component js file to index.html if you haven't done so already.
Also replace the current `<li>` in the index.html and add the new component:


`index.html`

```html
<!DOCTYPE html>
<html>
  <head>
    <title>Vue Workshop</title>
    <link
      rel="stylesheet"
      href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
      integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
      crossorigin="anonymous"
    />
    <link rel="stylesheet" href="./style.css" />
  </head>
  <body>
    <div id="app">
      <div class="container mt-5">
        <div class="row">
          <h1>{{title.toLocaleUpperCase() }}</h1>
          <ul>
            <beer v-for="beer in beers" :beer="beer" />
          </ul>
        </div>
      </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="./beer.js"></script>
    <script src="./app.js"></script>
  </body>
</html>
```

As you can see we bind the beer object in our `app-component` to our `beer-component`.

The `:beer` notation is a shorthand for `v-bind:beer` [v-bind](https://vuejs.org/v2/api/#v-bind)

Open op your browser and see the first result.

As you see with the title can directly call javascript functions to maintain the data.

## 1.2 Your first vue-experience?

If you would like to see the solution for the last part, checkout the `step-1.2` branch using the command:
`git checkout -f step-1.2`

How was your first experience with vue? Did you enjoy playing with it? Now, let's look at how we can use vue for more enterprise development.

## 2.0 Let's build real stuff now!

Vue comes with a great tool to setup your next web-project, the [Vue-cli](https://cli.vuejs.org/). The vue-cli enables you to define and generate your favourite project setup. You can choose the following things:

- Including `vue-router` router
- Including `vuex` state-mangement
- Use typescript yes or no
- Use `.vue` single-file-components yes or no
- Use .less, .sass, .scss or .css files.

You can try it out yourself following the [Vue-Cli](https://cli.vuejs.org/guide/creating-a-project.html#vue-create) documentation.

For now, we have already created a project setup. Today we want to focus on some aspects of Vue and not on the cli itself.

So first checkout the right starter branch for the next part of the workshop:

`git checkout step-2.0`

Run: `npm i` and `npm run serve` after it and follow the instructions on your command-terminal to see your first vue-app served on your localhost in the [browser](http://localhost:8080/)

The vue-cli uses Webpack as a build tool to create your components with `.vue` files.

As we have seen in the first part of the workshop it was quite tedious to create new components. The html in a string is very annoying for both your formatters and the code reviews. Today we will focus on the Vue functionalities. Most of the styling is separated into scss files. This is a design-choice, but not necessary.

We do not want to create a simple website. We want to make an application, including a sidebar and a header. In this part of the workshop we will be adding these to our app.

Let's start with the first steps.

After each step you can checkout the solution and run: `npm i` and `npm run serve`

As you can see, we will continue on the same subject: Beers!

First we will create a [new single file component](https://vuejs.org/v2/guide/single-file-components.html#ad): `Beer.vue` in the src/components directory.


Based on the examples in the single-file-component-documentation we can create our first component with a template-part and a script-part in the same file. Let's start with the first single file component:

`Beer.vue`

```html
<template>
  <li class="product-item">
    <div class="product-information">
      <Header class="product-title">{{ beer.title }}</header>
      <div>{{ beer.city }}</div>
      <img class="product-image" :src="beer.img" />
    </div>
  </li>
</template>
<script>
  export default {
    props: {
      beer: {}
    }
  };
</script>
```

As you can see we don't have a name defined in our component. Instead of a name we will 'export default'. In this way, the default of the component-name is based on the filename.

Now we can do the same as in our 1.0 workshop and use our component in our app-component.
Instead of importing the script-tag of the `beer.js`, we now need to import the component itself in App.vue and use it in the template part.

`App.vue`

```html
<template>
  <div id="app">
    <div>
      <Header />
      <div class="main-content">
        <ul class="main-list" v-if="beers">
          <Beer v-for="beer in beers" :beer="beer" :key="beer.id" />
        </ul>
      </div>
      <Sidebar :sidebarVisible="sidebarVisible" />
    </div>
  </div>
</template>
<script>
import Sidebar from "./components/Sidebar.vue";
import Header from "./components/Header.vue";
import Beer from "./components/Beer.vue";

export default {
  name: "app",
  data() {
    return {
      sidebarVisible: true,
      beers: [
        {
          id: "1",
          brewery: "brouwerij Jopen",
          title: "Mooie Nel I.P.A.",
          city: "Haarlem",
          province: "Noord-Holland",
          percentage: 6.5,
          img:
            "https://www.jopenbier.nl/app/uploads/2014/05/jopen-mooie-nel-1.png"
        },
        {
          id: "2",
          brewery: "brouwerij Jopen",
          title: "Malle Babbe",
          city: "Haarlem",
          province: "Noord-Holland",
          percentage: 5.5,
          img:
            "https://www.jopenbier.nl/app/uploads/2014/05/jopen-malle-babbe.png"
        }
      ]
    };
  },
  components: {
    Sidebar,
    Header,
    Beer
  }
};
</script>
<style lang="scss">
.main-list {
  display: flex;
  justify-content: center;
  padding: 0;
  margin: 0;
}

.main-content {
  margin-top: 60px;
}
</style>
```

You have just created your first single file component.

## 2.1 Your first single file component!

If you would like to see the solution for the previous part, checkout the `step-2.1` branch using the command:
`git checkout -f step-2.1`

Let's continue in our `Beer.vue` component and add some functionality.

`Beer.vue`

```html
<template>
  <li class="product-item">
    <div class="product-information">
      <header class="product-title">{{ beer.title }}</header>
      <div>{{ beer.brewery }}</div>
      <div>{{ beer.city }}</div>
      <img class="product-image" :src="beer.img" />
    </div>
    <div class="add-container">
      <button class="btn btn-primary" @click="decrementAmount">-</button>
      <span>{{ number }}</span>
      <button class="btn btn-primary" @click="incrementAmount">+</button>
      <button class="btn btn-primary" @click="handleAddToWishList">
        Add to wishlist
      </button>
    </div>
  </li>
</template>
<script>
export default {
  props: {
    beer: {}
  },
  data() {
    return {
      number: 0
    };
  },
  methods: {
    handleAddToWishList() {
      this.resetAmount();
    },
    resetAmount() {
      this.number = 0;
    },
    decrementAmount() {
      if (this.number > 0) {
        this.number--;
      }
    },
    incrementAmount() {
      this.number++;
    }
  }
};
</script>
<style lang="scss">
.main-list {
  display: flex;
  justify-content: center;
  padding: 0;
  margin: 0;
}

.main-content {
  margin-top: 60px;
}
</style>
```

As you can see we have implemented some methods in our beer component. We've used a nice [vue-shorthand: `@click`](https://vuejs.org/v2/guide/syntax.html#v-on-Shorthand)

For now the `handleAddToWishList` resets the `number` stupidly to 0. Later on, we will create a wish-list and update the state in that component.

## 2.2 Okay, not a full-blown web-app, but it's something, right?

If you would like to see the solution for the previous part, checkout the `step-2.2` branch using the command:
`git checkout -f step-2.2`

First let's make our sidebar functional. We would like to expand and collapse our sidebar with a wish counter indicator in the right corner.

Create a new single-file-component called: `WishCounter.vue`

For now it's just a template, because there is no amount yet:

`WishCounter.vue`

```html
<template>
  <div class="wish-counter">
    <img src="../assets/bottle.svg" />
    <div class="counter"></div>
  </div>
</template>
```

Next, we would like to include the new  `WishCounter.vue` in our `Header.vue` and create an event-emitter to communicate to the app component.

`Header.vue`

```html
<template>
  <header class="header">
    <a class="header-logo" href="#">
      <h1 class="header-title">
        {{ title.toLocaleUpperCase() }}
        <img src="../assets/logo.svg" alt="logo wish beer" />
      </h1>
    </a>
    <WishCounter @click.native="toggleSidebar" />
  </header>
</template>
<style lang="scss" scoped>
@import "../styles/components/_header.scss";
</style>
<script>
import WishCounter from "./WishCounter.vue";

export default {
  data() {
    return {
      title: "wish a beer"
    };
  },
  props: {
    sidebarVisible: Boolean
  },
  methods: {
    toggleSidebar() {
      this.$emit("toggle-sidebar");
    }
  },
  components: { WishCounter }
};
</script>
```

As you see we've used a @click directive on a vue-component, to make it functional we need to bind a [native click event](https://vuejs.org/v2/guide/components-custom-events.html#Binding-Native-Events-to-Components) to a component.


In the App component we will need to handle the [event-emitter](https://vuejs.org/v2/guide/components.html#Listening-to-Child-Components-Events) from the child-component `Header.vue` to toggle the sidebar.

And we need to toggle the sidebarVisible flag.

`App.vue`

```html
<template>
  <div id="app">
    <div>
      <Header @toggle-sidebar="toggleSidebar" />
      <div class="main-content">
        <ul class="main-list" v-if="beers">
          <Beer v-for="beer in beers" :beer="beer" :key="beer.id" />
        </ul>
      </div>
      <Sidebar :sidebarVisible="sidebarVisible" />
    </div>
  </div>
</template>
<script>
import Sidebar from "./components/Sidebar.vue";
import Header from "./components/Header.vue";
import Beer from "./components/Beer.vue";

export default {
  name: "app",
  data() {
    return {
      sidebarVisible: false,
      beers: [
        {
          id: "1",
          brewery: "brouwerij Jopen",
          title: "Mooie Nel I.P.A.",
          city: "Haarlem",
          province: "Noord-Holland",
          percentage: 6.5,
          img:
            "https://www.jopenbier.nl/app/uploads/2014/05/jopen-mooie-nel-1.png"
        },
        {
          id: "2",
          brewery: "brouwerij Jopen",
          title: "Malle Babbe",
          city: "Haarlem",
          province: "Noord-Holland",
          percentage: 5.5,
          img:
            "https://www.jopenbier.nl/app/uploads/2014/05/jopen-malle-babbe.png"
        }
      ]
    };
  },
  methods: {
    toggleSidebar() {
      this.sidebarVisible = !this.sidebarVisible;
    }
  },
  components: {
    Sidebar,
    Header,
    Beer
  }
};
</script>
<style lang="scss">
.main-list {
  display: flex;
  justify-content: center;
  padding: 0;
  margin: 0;
}

.main-content {
  margin-top: 60px;
}
</style>
```

We just created the first communication between components.

## 2.3 The first communication between components

If you would like to see the solution for the previous part, checkout the `step-2.3` branch using the command:
`git checkout -f step-2.3`

Finally, the first 'real' functionality. Let's get rid of the hardcoded data and setup an ajax-call to get some data.
We would like to consume a 'real' api now! The most popular approach to do this in Vue is with [Axios](https://vuejs.org/v2/cookbook/using-axios-to-consume-apis.html)

Stop your vue-app and run the following command in your terminal:

`npm i --save axios`

Next, we will need to add our Axios-instance to our Vue-instance to be able to use it.

In our `main.js` we import axios and add it to the vue instance:

`main.js`

```js
import Vue from "vue";
import App from "./App.vue";
import axios from "axios";

Vue.prototype.$http = axios;
Vue.config.productionTip = false;

new Vue({
  render: h => h(App)
}).$mount("#app");
```

After that we can remove the dummy-beers from our `App.vue` and use Axios to get our data and change the data property to an empty array. Besides that we need to call our new method on at the right moment. So we need a [VUE-lifecycle-method](https://vuejs.org/v2/api/#Options-Lifecycle-Hooks). For now we can use `created` to call our promise and bind our response to the beers data property.

`App.vue`

```html
<script>
import Sidebar from "./components/Sidebar.vue";
import Header from "./components/Header.vue";
import Beer from "./components/Beer.vue";

export default {
  name: "app",
  data() {
    return {
      sidebarVisible: false,
      beers: []
    };
  },
  created() {
    this.getBeers();
  },
  methods: {
    toggleSidebar() {
      this.sidebarVisible = !this.sidebarVisible;
    },
    getBeers() {
      const baseURI = `./beers.json`;
      this.$http.get(baseURI).then(result => {
        this.beers = result.data;
      });
    }
  },
  components: {
    Sidebar,
    Header,
    Beer
  }
};
</script>
<style lang="scss">
.main-list {
  display: flex;
  justify-content: center;
  padding: 0;
  margin: 0;
}

.main-content {
  margin-top: 60px;
}
</style>
```

## 2.4 The data is loaded. But how to maintain it?

If you would like to see the solution for the previous part, checkout the `step-2.4` branch using the command:
`git checkout step-2.4`

Until now we have not mutated any data!

We would like to add our favourite beers to the `wish-list` in the sidebar and keep our total amount up-to-date in the `wishCounter` in the header.
In a previous step we already saw the power of the emit-event to call and pass methods and data through components. But for data mutations we're going to want to use something that is more maintainable. Something that keeps our visual logic separated from the data.

Let's first add the Vue-specific state-manager.

Stop your vue-app and and run:

`npm install --save vuex`

Create a new file store.js in src

`store.js`

```js
import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {}
});
```

Change the `main.js` to use the store:

`main.js`

```js
import Vue from "vue";
import App from "./App.vue";
import axios from "axios";
import store from "./store";

Vue.prototype.$http = axios;
Vue.config.productionTip = false;

new Vue({
  store,
  render: h => h(App)
}).$mount("#app");
```

As you can read in the docs, the store is a container that holds your application state.
For the simple mutations we want to do today, we will use `mutations`. With `mutations` it is possible to directly change the state.
If there is asynchronous functionality involved, you could use the `actions` of your store.

We will create a first state and a getter for `countTotal` to read the state from the store.

`store.js`

```js
export default new Vuex.Store({
  state: {
    countTotal: 0
  },
  getters: {
    countTotal: state => state.countTotal
  },
  mutations: {
    incrementTotal(state, number) {
      state.countTotal = state.countTotal + number;
    }
  }
});
```

After that will implement a [getters](https://vuex.vuejs.org/guide/getters.html) to read the `totalCount` from the store in our `WishCounter.vue`.

`WishCounter.vue`

```html
<template>
  <div class="wish-counter">
    <img src="../assets/bottle.svg" />
    <div class="counter">{{ countTotal }}</div>
  </div>
</template>
<script>
  export default {
    computed: {
      countTotal() {
        return this.$store.state.countTotal;
      }
    }
  };
</script>
```

Now we will need to call our `mutation` `incrementTotal` in our `handleAddToWishList` method in `Beer.vue`. [mutation](https://vuex.vuejs.org/guide/mutations.html)

`Beer.vue`

```html
<script>
export default {
  props: {
    beer: {}
  },
  data() {
    return {
      number: 0
    };
  },
  methods: {
    handleAddToWishList() {
      this.$store.commit("incrementTotal", this.number);
      this.resetAmount();
    },
    resetAmount() {
      this.number = 0;
    },
    decrementAmount() {
      if (this.number > 0) {
        this.number--;
      }
    },
    incrementAmount() {
      this.number++;
    }
  }
};
</script>

```

## 2.5 We managed our state in Vue!

If you would like to see the solution for the previous part, checkout the `step-2.5` branch using the command:
`git checkout -f step-2.5`

Let's wrap up our functionality to update our wishlist.

First we need to extend our store. We would like to have a `mutation` to add a beer with an amount to our `wishedBeers` array.

`store.js`

```js
export default new Vuex.Store({
  state: {
    wishedBeers: [],
    countTotal: 0
  },
  getters: {
    countTotal: state => state.countTotal,
    wishedBeers: state => state.wishedBeers
  },
  mutations: {
    incrementTotal(state, number) {
      state.countTotal = state.countTotal + number;
    },

    addBeer(state, beer) {
      const exists = state.wishedBeers.some(w => {
        return w.id === beer.id;
      });
      if (exists) {
        state.wishedBeers.forEach(w => {
          if (w.id === beer.id) {
            w.number = w.number + beer.number;
          }
        });
      } else {
        state.wishedBeers = [...state.wishedBeers, beer];
      }
    }
  }
});
```

Now we are able to implement this functionality. Let's start in our `Beer.vue` and then make a call to the `mutation` in our store, and we can add the number to the beer count in the wish list.


`Beer.Vue`

```html
<script>
export default {
  props: {
    beer: {}
  },
  data() {
    return {
      number: 0
    };
  },
  methods: {
    handleAddToWishList() {
      this.$store.commit("addBeer", { ...this.beer, number: this.number });
      this.$store.commit("incrementTotal", this.number);
      this.resetAmount();
    },
    resetAmount() {
      this.number = 0;
    },
    decrementAmount() {
      if (this.number > 0) {
        this.number--;
      }
    },
    incrementAmount() {
      this.number++;
    }
  }
};
</script>
```

As you can see we can use the es6-spread operator over the objects.

Before we can test our new mutation we will have to add our wishlist to the sidebar.
We need to create a new component: Wish.vue

```html
<template>
  <li class="wish-item">
    <div>{{ wish.number }} x</div>
    <div class="wish-information">
      <Header class="wish-title">{{ wish.title }}</header>
      <span>{{ wish.brewery }}</span>
    </div>
    <img class="wish-image" :src="wish.img" />
  </li>
</template>
<script>
  export default {
    props: {
      wish: {}
    }
  };
</script>
```

As you can see we need a number on our beer-object to show the amount of beers.

Now we need to maintain our `Sidebar.vue` component to use the `wishedBeer getter` and use our new `Wish.vue`.

`Sidebar.vue`

```html
<template>
  <aside class="wish-list" :class="{ open: sidebarVisible }">
    <div class="wish-list-header">
      <h2>Your Wishes</h2>
    </div>
    <ul class="wishes">
      <Wish v-for="wish in wishedBeers" :wish="wish" :key="wish.id" />
    </ul>
  </aside>
</template>
<script>
  import Wish from "./Wish.vue";
  export default {
    props: {
      sidebarVisible: Boolean
    },
    components: {
      Wish
    },
    computed: {
      wishedBeers() {
        return this.$store.state.wishedBeers;
      }
    }
  };
</script>
```

By the usage of our Wish component in the template we see the [`:key`](https://vuejs.org/v2/guide/list.html#key) property. This property is used to give Vue a hint to modify or watch each item in an iteration.

## 2.6 We can actually do something in the app!

If you would like to see the solution for the previous part, checkout the `step-2.6` branch using the command:
`git checkout -f step-2.6`

Now we would like to enhance our app with a small change, but great vue functionality.

We want to add another way to close our sidebar. A cross-icon in the sidebar-corner.

`Sidebar.vue`

```html
<template>
  <aside class="wish-list" :class="{ open: sidebarVisible }">
    <div class="wish-list-header">
      <a class="close" @click="toggleSidebar" />
      <h2>Your Wishes</h2>
    </div>
    <ul class="wishes">
      <Wish v-for="wish in wishedBeers" :wish="wish" :key="wish.id" />
    </ul>
  </aside>
</template>
```

You can add the `<a class="close" @click="toggleSidebar" />` part as you can see above.

Now we need the same code as in our `Header.vue`

```js
methods: {
    toggleSidebar() {
      this.$emit("toggle-sidebar");
    }
}
```

And add the same event-emitter on our sidebar-component in our `App.vue`.

```html
<Sidebar @toggle-sidebar="toggleSidebar" :sidebarVisible="sidebarVisible" />
```

Of course this works fine, but it is not nice to duplicate the javascript code.
Luckily Vue brings us [mixins](https://vuejs.org/v2/guide/mixins.html)

We can use a mixin to reuse specific component-code in multiple components.

Let's create a new folder in our `src` folder called `mixins`

Than we create our mixin called: `toggleSidebar.js`

toggleSidebar.js:

```js
export const toggleSidebar = {
  props: {
    sidebarVisible: Boolean
  },
  methods: {
    toggleSidebar() {
      this.$emit("toggle-sidebar");
    }
  }
};
```

Now we are able to use our mixin in `Header.vue` and `Sidebar.vue`!

We add it to both components:

`Header.vue`
```html
...
<script>
import WishCounter from "./WishCounter.vue";
import { toggleSidebar } from "../mixins/toggleSidebar.js";

export default {
  data() {
    return {
      title: "wish a beer"
    };
  },
  mixins: [toggleSidebar],
  props: {
    sidebarVisible: Boolean
  },
  methods: {
    toggleSidebar() {
      this.$emit("toggle-sidebar");
    }
  },
  components: { WishCounter }
};
</script>
...
```

Sidebar.vue

```html
<script>
import Wish from "./Wish.vue";
import { toggleSidebar } from "../mixins/toggleSidebar.js";
export default {
  props: {
    sidebarVisible: Boolean
  },
  mixins: [toggleSidebar],
  components: {
    Wish
  },
  computed: {
    wishedBeers() {
      return this.$store.state.wishedBeers;
    }
  }
};
</script>
```

As you see we have removed the duplicate parts of the components.

## 2.7 That's not bad right?

If you would like to see the solution for the previous part, checkout the `step-2.7` branch using the command:
`git checkout -f step-2.7`

Let's add another small feature to our application.

We will add the clear-button in our sidebar to clear our wishlist. First we will add the button to the html. Than we will use the `@click` directive to the clear-button with a method to call a new `mutation` to reset the state properties. We would also like to use the the `v-if` directive to only show our button when our wishlist isn't empty.

Let's start to add the mutation in the store.js to clear the wishlist.

`store.js`

```js
import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);
Vue.use(axios);

export default new Vuex.Store({
  state: {
    wishedBeers: [],
    beers: [],
    countTotal: 0
  },
  getters: {
    countTotal: state => state.countTotal,
    wishedBeers: state => state.wishedBeers,
    beers: state => state.beers
  },
  mutations: {
    incrementTotal(state, number) {
      state.countTotal = state.countTotal + number;
    },

    resetWishedBeers(state) {
      state.countTotal = 0;
      state.wishedBeers = [];
    },

    initBeers(state, fetchedBeers) {
      state.beers = fetchedBeers;
    },

    addBeer(state, beer) {
      const exists = state.wishedBeers.some(w => {
        return w.id === beer.id;
      });
      if (exists) {
        state.wishedBeers.forEach(w => {
          if (w.id === beer.id) {
            w.number = w.number + beer.number;
          }
        });
      } else {
        state.wishedBeers = [...state.wishedBeers, beer];
      }
    }
  },
  actions: {
    async fetchBeers() {
      const baseURI = `./beers.json`;
      axios.get(baseURI).then(result => {
        this.commit("initBeers", result.data);
      });
    }
  }
});
```

Then we will use the `@click` directive to the clear-button with a method to call a new `mutation` to reset the state properties. We would also like to use the the `v-if` directive to only show our button when our wishlist isn't empty.

`Sidebar.vue`

```html
<template>
  <aside class="wish-list" :class="{ open: sidebarVisible }">
    <div class="wish-list-header">
      <a class="close" @click="toggleSidebar" />
      <h2>Your Wishes</h2>
    </div>
    <div class="button-container">
      <button
        v-if="wishedBeers.length > 0"
        @click="handleClearWishList"
        class="btn btn-primary"
      >
        Clear Wishlist
      </button>
    </div>
    <ul class="wishes">
      <Wish v-for="wish in wishedBeers" :wish="wish" :key="wish.id" />
    </ul>
  </aside>
</template>
<script>
import Wish from "./Wish.vue";
import { toggleSidebar } from "../mixins/toggleSidebar.js";
export default {
  props: {
    sidebarVisible: Boolean
  },
  mixins: [toggleSidebar],
  components: {
    Wish
  },
  methods: {
    handleClearWishList() {
      this.$store.commit("resetWishedBeers");
    }
  },
  computed: {
    wishedBeers() {
      return this.$store.state.wishedBeers;
    }
  }
};
</script>
```


The next step is to change our webpage to a single-page-application. We can achieve this with the [Vue Router](https://router.vuejs.org/guide/#html)

Add the vue router by running the following command in our terminal:

`npm i save vue-router`

Create a new folder in our src folder called: `views`.

Now we will add our router file to the root of the project, called `router.js`:

`router.js`
```js
import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/the-end",
      name: "the-end",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/TheEnd.vue")
    }
  ]
});

```

And we add our router to our view app in `main.js`:

```js

import Vue from "vue";
import App from "./App.vue";
import axios from "axios";
import store from "./store";
import router from "./router";

Vue.prototype.$http = axios;
Vue.config.productionTip = false;

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount("#app");


```

Now we need to move our dynamic-code from the App.vue that we had before to a new view-component called `Home.vue` in the folder `views` to get it working again.
Create another view-component to views called: `TheEnd.vue` and change the file to: The result of the re-shuffling of the code results in the following `App.vue` template:

`App.vue`

```html
<template>
  <div id="app">
    <Header
      @toggle-sidebar="toggleSidebar"
      :sidebarVisible="sidebarVisible"
    ></header>
    <router-view></router-view>
    <Sidebar @toggle-sidebar="toggleSidebar" :sidebarVisible="sidebarVisible" />
  </div>
</template>
```

`App.vue`

```html
<template>
  <div id="app">
    <Header
      @toggle-sidebar="toggleSidebar"
      :sidebarVisible="sidebarVisible"
    ></header>
    <router-view></router-view>
    <Sidebar @toggle-sidebar="toggleSidebar" :sidebarVisible="sidebarVisible" />
  </div>
</template>

<script>
  import Sidebar from "./components/Sidebar.vue";
  import Header from "./components/Header.vue";

  export default {
    name: "app",
    data() {
      return {
        sidebarVisible: false
      };
    },
    methods: {
      toggleSidebar() {
        this.sidebarVisible = !this.sidebarVisible;
      }
    },
    components: {
      Sidebar,
      Header
    }
  };
</script>
```

`Home.vue`

```html
<template>
  <div>
    <div class="main-content">
      <ul class="main-list" v-if="beers">
        <Beer v-for="beer in beers" :beer="beer" :key="beer.id" />
      </ul>
    </div>
  </div>
</template>
<script>
  import Beer from "../components/Beer.vue";

  export default {
    data() {
      return {
        beers: []
      };
    },
    created() {
      this.getBeers();
    },
    methods: {
      getBeers() {
        const baseURI = `./beers.json`;
        this.$http.get(baseURI).then(result => {
          this.beers = result.data;
        });
      }
    },
    components: {
      Beer
    }
  };
</script>
<style lang="scss">
  .main-list {
    display: flex;
    justify-content: center;
    flex-wrap: wrap;
    padding: 0;
    margin: 0;
  }

  .main-content {
    margin-top: 60px;
  }
</style>
```

`TheEnd.vue`

```html
<template>
  <div class="the-end">
    <h1>The End</h1>
  </div>
</template>
```

Now we will create a new component called: `Menu.vue`

`Menu.vue`

```html
<template>
  <ul class="menu">
    <li>
      <router-link class="nav-link" to="/">Beers</router-link>
    </li>
    <li>
      <router-link class="nav-link" to="/the-end">The End</router-link>
    </li>
  </ul>
</template>
```

We will add some router-links to it to create a menu in our header.
Include the new menu component in our Header component:

`Header.vue`

```html
<template>
  <header class="header">
    <a class="header-logo" href="#">
      <h1 class="header-title">
        {{ title.toLocaleUpperCase() }}
        <img src="../assets/logo.svg" alt="logo wish beer" />
      </h1>
    </a>
    <Menu />
    <WishCounter @click.native="toggleSidebar" />
  </header>
</template>
<style lang="scss" scoped>
@import "../styles/components/_header.scss";
</style>

<script>
import WishCounter from "./WishCounter.vue";
import Menu from "./Menu.vue";
import { toggleSidebar } from "../mixins/toggleSidebar.js";

export default {
  data() {
    return {
      title: "wish a beer"
    };
  },
  mixins: [toggleSidebar],
  props: {
    sidebarVisible: Boolean
  },
  methods: {
    toggleSidebar() {
      this.$emit("toggle-sidebar");
    }
  },
  components: { WishCounter, Menu }
};
</script>
```

Before we take a look at our result, we will refactor our code a bit. The ajax call now is done in our Home component. To fetch data and use our state we can move this part to Vuex and use our application state.

We start with the refactoring of store.js

`store.js`
```js
import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);
Vue.use(axios);

export default new Vuex.Store({
  state: {
    wishedBeers: [],
    beers: [],
    countTotal: 0
  },
  getters: {
    countTotal: state => state.countTotal,
    wishedBeers: state => state.wishedBeers,
    beers: state => state.beers
  },
  mutations: {
    incrementTotal(state, number) {
      state.countTotal = state.countTotal + number;
    },

    resetWishedBeers(state) {
      state.countTotal = 0;
      state.wishedBeers = [];
    },

    initBeers(state, fetchedBeers) {
      state.beers = fetchedBeers;
    },

    addBeer(state, beer) {
      const exists = state.wishedBeers.some(w => {
        return w.id === beer.id;
      });
      if (exists) {
        state.wishedBeers.forEach(w => {
          if (w.id === beer.id) {
            w.number = w.number + beer.number;
          }
        });
      } else {
        state.wishedBeers = [...state.wishedBeers, beer];
      }
    }
  },
  actions: {
    fetchBeers() {
      const baseURI = `./beers.json`;
      axios.get(baseURI).then(result => {
        this.commit("initBeers", result.data);
      });
    }
  }
});
```

For now we don't need our axios implementation anymore in our Vue instance, so let's remove it.

`main.js`
```js
import Vue from "vue";
import App from "./App.vue";
import store from "./store";
import router from "./router";

Vue.config.productionTip = false;

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount("#app");
```

Now let's fix the errors, and change our Home.vue


`Home.vue`

```html
<template>
  <div>
    <div class="main-content">
      <ul class="main-list" v-if="beers">
        <Beer v-for="beer in beers" :beer="beer" :key="beer.id" />
      </ul>
    </div>
  </div>
</template>
<script>
import Beer from "../components/Beer.vue";

export default {
  created() {
    this.getBeers();
  },
  methods: {
    getBeers() {
      this.$store.dispatch("fetchBeers");
    }
  },
  computed: {
    beers() {
      return this.$store.state.beers;
    }
  },
  components: {
    Beer
  }
};
</script>
<style lang="scss">
.main-list {
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  padding: 0;
  margin: 0;
}

.main-content {
  margin-top: 60px;
}
</style>
```

As you can see we've used the [dispatch](https://vuex.vuejs.org/guide/actions.html) to call an action in our VuexStore.


Now let's open our browser and see what we have created.

## 2.8 The End

If you would like to see the solution for the previous part, checkout the `step-2.8` branch using the command:
`git checkout -f step-2.8`

We hope you enjoyed the workshop. We hope that you will feel a bit more familiar with the Vue-library.
Of course there are a lot of alternatives in setup. We are more than happy to discuss this with you.
